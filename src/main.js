import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueMoment from 'vue-moment'
import moment from 'moment-timezone'
import VueSocketIO from 'vue-socket.io'
import SocketIO from 'socket.io-client'
import 'flag-icon-css/sass/flag-icon.scss'
import VueChatScroll from 'vue-chat-scroll'
Vue.use(VueChatScroll)
Vue.config.productionTip = false

Vue.use(VueMoment, { moment })
Vue.use(VueAxios, axios)

axios.defaults.baseURL = 'https://messenger-backend-rd.herokuapp.com';
// axios.defaults.withCredentials = true;
axios.defaults.headers.common['Authorization'] = localStorage.getItem('Authorization');

axios.interceptors.response.use(undefined, (error) => {

  if (error.response.status !== 401) {
    return new Promise((resolve, reject) => {
      reject(error);
    });
  }

  // Logout user if token refresh didn't work or user is disabled
  if (error.response.status == 401 && error.response.statusText == 'Unauthorized') {
    store.dispatch("invalidToken");
    return new Promise((resolve, reject) => {
      reject(error);
    });
  }

});

const options = {
  reconnectionDelayMax: 10000,
  // path: '/chat',
  credentials: true,
  auth: { token: store.getters.token },
}
Vue.use(new VueSocketIO({
  debug: false,
  connection: SocketIO('https://messenger-backend-rd.herokuapp.com', options), //options object is Optional
  vuex: {
    store,
    actionPrefix: "SOCKET_",
    mutationPrefix: "SOCKET_"
  },
})
);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')

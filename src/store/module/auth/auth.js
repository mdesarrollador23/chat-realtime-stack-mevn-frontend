import axios from 'axios'
import router from '@/router'

export default {
    state: {
        token: null,
        user: null,
    },
    getters: {
        token: state => {
            return state.token;
        },
        user: state => {
            return state.user;
        },
    },
    mutations: {
        setToken(state, payload) {
            state.token = payload;
        },
        setUser(state, payload) {
            state.user = payload;
        },
    },
    actions: {
        invalidToken: ({
            commit
        }) => {
            localStorage.removeItem("Authorization");
            commit('setToken', null);
            commit('setUser', null);
            axios.defaults.headers.common['Authorization'] = null;
            router.push("/auth");
        },
        async signup({
            commit,
            dispatch
        }, user) {
            await axios.post('/signup', user).then(res => {
                setTimeout(() => {
                    localStorage.setItem('Authorization', `Bearer ${res.data.token}`)
                    commit('setToken', `Bearer ${res.data.token}`);
                    commit('setUser', res.data.user);
                    dispatch('getUpdateToken');
                    axios.defaults.headers.common['Authorization'] = localStorage.getItem('Authorization');
                    router.push('/chat');
                }, 1000);
            }).catch(e => {
                throw new Error(e.response.data.msg)
            })
        },
        async signin({
            commit,
            dispatch
        }, user) {
            await axios.post('/signin', user).then(res => {
                setTimeout(() => {
                    localStorage.setItem('Authorization', `Bearer ${res.data.token}`)
                    commit('setToken', `Bearer ${res.data.token}`);
                    commit('setUser', res.data.user);
                    dispatch('getUpdateToken');
                    axios.defaults.headers.common['Authorization'] = localStorage.getItem('Authorization');
                    router.push('/chat');
                }, 1000);
            }).catch(e => {
                throw new Error(e.response.data.msg)
            })
        },
        getUpdateToken({
            commit
        }) {
            if (localStorage.getItem('Authorization')) {
                commit('setToken', localStorage.getItem('Authorization'));
            } else {
                commit('setToken', null);
                commit('setUser', null);
            }
        },
        logout({
            commit
        }) {
            localStorage.removeItem("Authorization");
            commit('setToken', null);
            // commit('setUser', null);
            axios.defaults.headers.common['Authorization'] = null;
            router.push("/auth");
        }
    }
}
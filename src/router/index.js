import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: "*",
    name: "error",
    component: () => import("@/errors/Error404.vue"),
  },
  //Chat
  {
    path: "/",
    name: "home",
    component: () => import('@/views/chat/Home.vue'),
    redirect: { name: "chat" },
    meta: {
      requiresAuth: true,
    },
    children: [
      {
        path: "/chat",
        name: "chat",
        component: () => import('@/views/chat/Chat.vue'),
      }
    ]

  },
  //Authentication
  {
    path: "/auth",
    name: "auth",
    component: () => import('@/views/auth/Auth.vue'),
    redirect: { name: "signin" },
    meta: {
      requiresAuth: false,
    },
    children: [
      {
        path: "/auth/signin",
        name: "signin",
        component: () => import('@/views/auth/SignIn.vue'),
      },
      {
        path: "/auth/signup",
        name: "signup",
        component: () => import('@/views/auth/SignUp.vue'),
      },
      {
        path: "/auth/forget",
        name: "forget",
        component: () => import('@/views/auth/ForgetPassword.vue'),
      }
    ]

  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  // let documenttitle = `${process.env.VUE_APP_TITLE} - ${to.meta.title}`;
  // document.title = documenttitle;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  if (requiresAuth && store.getters.token == null) {
    next({
      name: 'auth'
    });
  } else {
    next();
  }

})

export default router
